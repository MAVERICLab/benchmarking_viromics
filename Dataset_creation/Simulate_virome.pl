#!/usr/bin/env perl
use strict;
use autodie;
use Getopt::Long;
use Cwd;
use Math::Round;
use EnvironmentModules;
my $h='';
my $cmd='';
my $out='';
my $code='';
my $compo_file='';
my $nb_reads_sample='';
GetOptions ('help' => \$h, 'h' => \$h, 'i=s'=>\$compo_file, 'c=s'=>\$code, 'r=s'=>\$nb_reads_sample);
if ($h==1 || $compo_file eq "" || $code eq "" || $nb_reads_sample eq ""){ # If asked for help or did not set up any argument
	print "# Script to simulate a virome from a composition file
# Arguments :
# -i : composition file
# -c : code of the dataset
# -r : number of reads to simulate in the dataset
";
	die "\n";
}
## Pick your out dir
# my $out_dir_root="Simulations_10M/";
# my $out_dir_root="Simulations_1M/";
my $out_dir_root="Simulations_100k/";

my $path_nessm="tools/NeSSM/NeSSM/NeSSM_CPU/NeSSM";
my $path_index="tools/NeSSM/databases/NeSSM_RefSeqPhage_v80/index";

my %store;
my $total=0;
print "Reading $compo_file\n";
open my $tsv,"<",$compo_file;
while(<$tsv>){
	chomp($_);
	if ($_=~/^##/){next;}
	my @tab=split("\t",$_);
	$store{$tab[0]}{"name_1"}=$tab[1];
	$tab[1]=~s/\s/_/g;
	$store{$tab[0]}{"name_2"}=$tab[1];
	$store{$tab[0]}{"relab"}=$tab[2];
	$total+=$tab[2];
}
close $tsv;

print "Loading all relevant sequences\n";
my $db_root="tools/NeSSM/databases/NeSSM_RefSeqPhage_v80/";
foreach my $genome (keys %store){
	my $name=$store{$genome}{"name_2"};
	my $dir=$db_root.$name."/";
	my @files=<$dir/*.fna>;
	if ($#files==-1){
		print "!!!!!! NO SEQUENCE IN DATABASE FOR $genome ($name) \n";
		<STDIN>;
		die("");
	}
	foreach my $fasta (@files){
		$store{$genome}{"file"}=$fasta;
		$fasta=~/.*\/([^\/]+)\.fna/;
		$store{$genome}{"id"}=$1;
		open my $fa,"<",$fasta;
		while(<$fa>){
			chomp($_);
			if ($_=~/^>/){}
			else{$store{$genome}{"seq"}.=$_;}
		}
		close $fa;
	}
}

print "Ok, all genomes found\n";
my $out_dir=$out_dir_root.$code;
&run_cmd("mkdir $out_dir");

my $tmp_root="tmp";
my $tmp_1="tmp_1.fq";
my $tmp_2="tmp_2.fq";
my $compo="compo.tsv";
my $out_read_1=$out_dir."/Reads_raw_R1.fastq.gz";
my $out_read_2=$out_dir."/Reads_raw_R2.fastq.gz";
if (-e $out_read_1){`rm $out_read_1`;}
if (-e $out_read_2){`rm $out_read_2`;}
my $fasta_clean=$out_dir."/Refs_clean.fna";
my $true_coverage=$out_dir."/Refs_coverage.csv";
open my $s1,">",$fasta_clean;
open my $s2,">",$true_coverage;
my $total_done;
print $s2 "## Genome,File,Length,Th rel. ab.,Real # reads\n";
my @tab_genomes=sort {$store{$a}{"relab"} <=> $store{$b}{"relab"}} keys %store;
foreach (my $i=0;$i<=$#tab_genomes;$i++){
	my $genome=$tab_genomes[$i];
	print "== Sampling $genome - $store{$genome}{name_1} - $store{$genome}{name_2}\n";
	my $nb_reads=round($store{$genome}{"relab"}/$total*$nb_reads_sample);
	if ($i==$#tab_genomes){
		print "\tAdjusting the number of reads to get exactly at the requested level, can be slightly off due to rounding\n";
		print "\tOriginal # reads: $nb_reads\n";
		$nb_reads=$nb_reads_sample-$total_done;
		print "\tCorrected # reads: $nb_reads\n";
	}
	if ($nb_reads==0){
		print "\tNo reads sampled for this genome, we don't generate any reads\n";
	}
	else{
		open my $tmp,">",$compo;
		print $tmp "$store{$genome}{name_1}\t1\n";
		close $tmp;
		my $tag=0;
		while($tag==0){
			&run_cmd("$path_nessm -r $nb_reads -list $compo -index $path_index -m illumina -e 1 -o $tmp_root -l 100");
			my $test=`wc -l $tmp_1`;
			chop($test);
			my $test_2=`wc -l $tmp_2`;
			chop($test_2);
			$test+=$test_2;
			$test/=8;
			if ($test == $nb_reads){
				# Everything ok, we add this to the main fastq file
				$tag=1;
				&run_cmd("gzip -c $tmp_1 >> $out_read_1");
				&run_cmd("gzip -c $tmp_2 >> $out_read_2");
			}
			else{
				print "!! Pblm, we wanted $nb_reads reads, and we counted $test\n";
				print "so we redo\n";
			}
		}
		$total_done+=$nb_reads;
	}
	print $s1 ">".$store{$genome}{"name_2"}."\n";
	print $s1 $store{$genome}{"seq"}."\n";
	print $s2 $store{$genome}{"name_2"}.",".$store{$genome}{"file"}.",".length($store{$genome}{"seq"}).",".$store{$genome}{"relab"}/$total.",".$nb_reads."\n";
}



sub run_cmd{
	my $cmd=$_[0];
	print "$cmd\n";
	my $out=`$cmd`;
	print "$out\n";
	return($out);
}
