#!/usr/bin/env perl
use strict;
use autodie;
use Getopt::Long;
use Custom::Utils;
use Bio::SeqIO;
use Math::Round;
use Cwd;
use EnvironmentModules;
module("load megahit/1.0.6");
module("load velvet/1.2.07_1");
# module("load idba/1.1.1");
module("unload spades");
module("load spades/3.10.0");
module("unload gcc");
module("load gcc/6.3.0");
module("load python");
module("load ruby");
module("load blast+");
module("load mummer");
my $h='';
my $cmd='';
my $out='';
my $code='';
my $path_nessm="tools/NeSSM/NeSSM/NeSSM_CPU/NeSSM";
GetOptions ('help' => \$h, 'h' => \$h);
if ($h==1 || $ARGV[0] eq ''){ # If asked for help or did not set up any argument
	print "# Script to simulate microdiversity
# Arguments :
# toto
";
	die "\n";
}

my %decode=("302861121"=>"939482425","77020191"=>"1070106504","206600290"=>"1070620113","472342280"=>"590236203","730977575"=>"1102616531","530787156"=>"610585404","431810092"=>"1006160387","40807285"=>"939482335","472339167"=>"1070621080","408905857"=>"971763756","45429984"=>"971763747","157102938"=>"939535432","509139223"=>"712913869","109392287"=>"939482383","472341879"=>"662562505","269838908"=>"939482392","514050383"=>"939535554","543171167"=>"1075322144","509141536"=>"966200071","543171494"=>"1070106186","695227973"=>"971753555","20065797"=>"1070640594","9635581"=>"939482395","472341743"=>"662562498","543170722"=>"1070639280","160693214"=>"939482390","189043087"=>"1070639546","50058548"=>"939482427","47566140"=>"906476365","195546641"=>"1070639874","563399740"=>"1070619317","302393080"=>"1070638039","157102937"=>"939489505","508178651"=>"1070639165","525972254"=>"971760483","563398109"=>"608309541","461475022"=>"985759046","570033450"=>"939535472","543170620"=>"1070102247","429216174"=>"985760442","31343652"=>"939482423");



# Load relevant info
my $out_dir="Simu_Viromes/Microdiversity_v3/";

my %store_input;
my @list;
my $sample_in="Sample_1_comp.tsv";
my $total=0;
open my $tsv,"<",$sample_in;
while(<$tsv>){
	chomp($_);
	if ($_=~/^##/){next;}
	my @tab=split("\t",$_);
	if (defined($decode{$tab[0]})){$tab[0]=$decode{$tab[0]};}
	$store_input{$tab[0]}{"name_1"}=$tab[1];
	push(@list,$tab[1]);
	$tab[1]=~s/\s/_/g;
	$store_input{$tab[0]}{"name_2"}=$tab[1];
	$store_input{$tab[0]}{"relab"}=$tab[2];
	$total+=$tab[2];
}
close $tsv;




# Load all sequences and gene coordinates for phages 
my $gb_file="databases/RefseqVirus/2017-01-31_Release_80/phage.genomic.gbff.gz";
my $gb_file_red="Red_gb_microvid.gb";
my $fh;
my $tag_gb=0;
if (-e $gb_file_red){open $fh,"<",$gb_file_red; $tag_gb=1;}
else{open $fh,"gunzip -c $gb_file |";}
my $seqio_obj= Bio::SeqIO->new(-fh => $fh);
print "Loading all sequences and gene coordinates\n";
my %store;
my %store_gene;
my %check;
while (my $seq_obj=$seqio_obj->next_seq){
	my $id=$seq_obj->accession;
	my $pid=$seq_obj->primary_id;
	my $genome=$pid;
# 	my @acc = $seq_obj->get_secondary_accessions();
	my $genome_name=$seq_obj->species->node_name;
	print "$genome === $id === $pid === $genome_name\n";
# 	<STDIN>;
	my $tag=0;
	if (defined($store_input{$pid})){
		$tag=1;
		$check{$pid}=1;
	}
# 	foreach my $id_genome (@list){
# 		if ($id_genome eq $genome){
# 			$tag=1;
# 			$check{$genome}=1;
# 		}
# 	}
	if ($tag==0){next;}
	if ($tag_gb==0){
		my $seqio_obj= Bio::SeqIO->new(-file => ">>$gb_file_red", -format=>"genbank");
		$seqio_obj->write_seq($seq_obj);
		$seqio_obj->close();
	}
	$store{$genome}{"seq"}=$seq_obj->primary_seq->seq;
	$store{$genome}{"name_3"}=$genome;
# 	print "$genome -> $store{$genome}{seq}\n";
# 	<STDIN>;
	my @cds_features = grep { $_->primary_tag eq 'CDS' } $seq_obj->get_SeqFeatures;
	foreach my $cds (@cds_features){
# 		print $cds->start." to ".$cds->end."\n";
		$store_gene{$genome}{$cds->start}=$cds->end;
		my $n=0;
		for (my $i=$cds->start;$i<=$cds->end;$i++){
			$n++;
			if ($n % 3 == 0 ) { $store{$genome}{"position"}{$i}="wobble"; }
			else { $store{$genome}{"position"}{$i}="conserved"; }
		}
	}
}
close $fh;

foreach my $genome (keys %store_input){
	if ($check{$genome}==1){}
	else{
		print "?!?!?! We did not find $genome ".$store_input{$genome}{"name_1"}."\n";
	}
	if (!defined($store{$genome}{"seq"}) || length($store{$genome}{"seq"})==0){
		print "!?!?!?! $genome has no sequence ?\n";
		print $store{$genome}{"seq"}."\n";
		die("dying here arrrrgh\n");
	}
}





my $nb_reads_sample=10000000;

my %info_microdiv; # Decide the parameters of each population now
my $out_file_pops=$out_dir."Populations_info.csv";
open my $s1,">",$out_file_pops;
print $s1 "Id,Genome,Genome_2,Length,Coverage,N_population,Mutation_rate,Population_shape\n";
# Varying parameters
# 		-> coverage - determined from the virome sampling
# 		-> # of strains - 10, 50, 500
my @tab_pops=(10,50,100);
# 		-> mutation rate for variable position generating each population - 20-40%, 40-60%, 60-80%
# 		-> mutation rate for "conserved" position will be the variable rate/100
my @tab_mut_rate=(5,10,20);
# 		-> population shape, through pl parameter - 0.1, 10, 100, 1000
my @tab_pop_shape=(0.1,1,10,100,1000);

foreach my $genome (sort keys %store_input){
	my $nb_reads=round($store_input{$genome}{"relab"}/$total*$nb_reads_sample); # This will be the coverage
	if ($nb_reads==0){next;}
	if (!defined($store{$genome}{"seq"}) || length($store{$genome}{"seq"})==0){
		print "!?!?!?! $genome has no sequence ?\n";
		print $store{$genome}{"seq"}."\n";
		die("dying here arrrrgh\n");
	}
	my $cover=$nb_reads*200/length($store{$genome}{"seq"});
	&fisher_yates_shuffle(\@tab_pops);
	&fisher_yates_shuffle(\@tab_mut_rate);
	&fisher_yates_shuffle(\@tab_pop_shape);
	print $s1 $genome.",".$store_input{$genome}{"name_1"}.",".$store_input{$genome}{"name_2"}.",".length($store{$genome}{"seq"}).",".$cover.",".$tab_pops[0].",".$tab_mut_rate[0].",".$tab_pop_shape[0]."\n";
	$info_microdiv{$genome}{"nb_reads"}=$nb_reads;
	$info_microdiv{$genome}{"n_pop"}=$tab_pops[0];
	$info_microdiv{$genome}{"mut_rate"}=$tab_mut_rate[0];
	$info_microdiv{$genome}{"pop_shape"}=$tab_pop_shape[0];
}
close $s1;


my $out_read_1=$out_dir."MetaG_R1.fastq.gz";
my $out_read_2=$out_dir."MetaG_R2.fastq.gz";
my $tmp_root=$out_dir."microdiv_reads";
my $tmp_1=$out_dir."microdiv_reads_1.fq";
my $tmp_2=$out_dir."microdiv_reads_2.fq";
my $compo="tmpcompmicrodiv.tsv";

my $ref_genome=$out_dir."Ref_genomes.fna";
my %store_additional;
foreach my $genome (sort keys %info_microdiv){
	my $n_strain=$info_microdiv{$genome}{"n_pop"};
	print "Generating $n_strain populations for $genome - $store_input{$genome}{name_2}\n";
	my $fna_dir=$out_dir."/Tmp/";
	my $index_file=$out_dir."/Index_tmp";
	my $l=length($store{$genome}{"seq"});
	# select the 2 genes that will be under positive selection
	my %mutable_positions=%{$store{$genome}{"position"}};
	my @tab_genes=keys %{$store_gene{$genome}};
	my %check_g=();
	for (my $i=1;$i<=2;$i++){
		my $n=rand()*$#tab_genes;
		while ($check_g{$n}==1){$n=rand()*$#tab_genes;}
		for (my $j=$tab_genes[$n];$j<=$store_gene{$genome}{$tab_genes[$n]};$j++){
			$mutable_positions{$j}="wobble";
		}
		$check_g{$n}=1;
	}
	my @list_strains;
	open my $in,">",$index_file;
	for (my $i=1;$i<=$n_strain;$i++){
		# generate another member of the population
		my $r=$info_microdiv{$genome}{"mut_rate"}/100;
		print "\tGenome $i -> mutation rate is $r\n";
		my $new_seq=&create_sequence($store{$genome}{"seq"},$store{$genome}{"position"},$r);
		my $out_file=$fna_dir."/Genome_".$i.".fna";
		open my $s1,">",$out_file;
		print $s1 ">Genome_".$i."\n";
		print $s1 $new_seq."\n";
		close $s1;
		push(@list_strains,"Genome_".$i);
		print $in "$i\t$l\t0\tGenome_$i\t$out_file\n";
	}
	close $in;
	print "Now calculating the average ANI within the population\n";
	open my $s1,">",$ref_genome;
	print $s1 ">$genome\n$store{$genome}{seq}\n";
	close $s1;
	$store_additional{$genome}{"ani"}=&average_ani($fna_dir,$ref_genome);
	print "\tPopulation from $genome has $store_additional{$genome}{ani} % ANI to consensus on average\n";
	print "Now decding the relative abundance of each strain with a power-law of parameter $info_microdiv{$genome}{pop_shape}\n";
	my @tab_cover=@{&get_power_distrib($n_strain,$info_microdiv{$genome}{"pop_shape"})};
	# Put this info in a csv file
	my $max=0;
	my $shannon=0;
	for (my $j=0;$j<=$#tab_cover;$j++){
		print "$list_strains[$j] -> $tab_cover[$j]\n";
		if ($tab_cover[$j]>$max){$max=$tab_cover[$j];}
		if ($tab_cover[$j]>0){$shannon+=$tab_cover[$j]*log($tab_cover[$j]);}
	}
	$shannon*=-1;
	$store_additional{$genome}{"max"}=$max;
	$store_additional{$genome}{"shannon"}=$shannon;
	@tab_cover=reverse(@tab_cover);

	my $n_reads=$info_microdiv{$genome}{"nb_reads"};
	print "Now generating $n_reads reads for this genome $genome\n";
	if (-e $tmp_1){`rm $tmp_1`;}
	if (-e $tmp_2){`rm $tmp_2`;}
	my $total_done=0;
	for (my $j=0;$j<=$#tab_cover;$j++){
		my $strain=$list_strains[$j];
		print "== Sampling $strain\n";
		my $nb_reads=round($tab_cover[$j]*$n_reads);
		if ($j==$#list_strains){
			$nb_reads=$n_reads-$total_done;
		}
		if ($nb_reads<=0){
			print "\tNo reads sampled for this genome, we don't generate any reads\n";
		}
		else{
			open my $tmp,">",$compo;
			print $tmp "$strain\t1\n";
			close $tmp;
			my $tag_simu=0;
			while($tag_simu==0){
				&run_cmd("$path_nessm -r $nb_reads -list $compo -index $index_file -m illumina -e 1 -o $tmp_root -l 100");
				my $test=`wc -l $tmp_1`;
				chop($test);
				my $test_2=`wc -l $tmp_2`;
				chop($test_2);
				$test+=$test_2;
				$test/=8;
				if ($test == $nb_reads){$tag_simu=1;}
				else{
					print "!! Pblm, we wanted $nb_reads reads, and we counted $test\n";
					print "We try again\n";
				}
			}
			# Everything ok, we add this to the main fastq file
			&run_cmd("gzip -c $tmp_1 >> $out_read_1");
			&run_cmd("gzip -c $tmp_2 >> $out_read_2");
		}
	}
	&run_cmd("rm $fna_dir/*.fna $index_file");
}


my $out_file_pops_2=$out_dir."Populations_info_additional.csv";
open my $s1,">",$out_file_pops_2;
open my $csv,"<",$out_file_pops;
while(<$csv>){
	chomp($_);
	my @tab=split(",",$_);
	if ($tab[0] eq "Id"){ print $s1 $_.",Shannon,Dominant,ANI\n";}
	else{print $s1 $_.",".$store_additional{$tab[0]}{"shannon"}.",".$store_additional{$tab[0]}{"max"}.",".$store_additional{$tab[0]}{"ani"}."\n";}
}
close $csv;


sub create_sequence{
	my $ori_seq=$_[0];
	my %positions=%{$_[1]};
	my $rate=$_[2];
	my $rate_conserved=$rate/100;
	my @t=split("",$ori_seq);
	for (my $i=0;$i<=$#t;$i++){
		if ((!defined($positions{$i})) || ($positions{$i} eq "wobble")){
			# Potentially mutated
			my $r=rand();
			if ($r<$rate){
				$t[$i]=&mutate($t[$i]);
			}
		}
		else{
			my $r=rand();
			if ($r<$rate_conserved){
				$t[$i]=&mutate($t[$i]);
			}
		}
	}
	my $new_seq=join("",@t);
	return($new_seq);
}

sub mutate {
	my $nuc=$_[0];
	my $r=rand();
	if ($nuc eq "A"){
		if ($r<=0.5){$nuc="T";}
		elsif($r<=0.75){$nuc="G";}
		else{$nuc="C";}
	}
	elsif ($nuc eq "T"){
		if ($r<=0.5){$nuc="A";}
		elsif($r<=0.75){$nuc="G";}
		else{$nuc="C";}
	}
	elsif ($nuc eq "G"){
		if ($r<=0.5){$nuc="C";}
		elsif($r<=0.75){$nuc="A";}
		else{$nuc="T";}
	}
	elsif ($nuc eq "C"){
		if ($r<=0.5){$nuc="G";}
		elsif($r<=0.75){$nuc="A";}
		else{$nuc="T";}
	}
	return ($nuc);
}


sub average_ani{
	my @list=<$_[0]/*.fna>;
	my $ref=$_[1];
	my $out_tmp="tmp_ani.coords";
	my $n=0;
	my $total=0;
	for (my $i=0;$i<=$#list;$i++){
		my $g1=$list[$i];
		my $out=`java -jar OAU.jar -f1 $g1 -f2 $ref -o $out_tmp -u usearch/7.0.959/bin/usearch`;
		open my $txt,"<",$out_tmp;
		my $tagtag=0;
		while(<$txt>){
			chomp($_);
			my @tab=split("\t",$_);
			if ($tagtag==0){
				if ($tab[1] eq "orthoANI_value"){$tagtag=1;}
			}
			elsif($tagtag==1){
				$total+=$tab[1];
				$n++;
			}
		}
		close $txt;
	}
	$total/=$n;
	return $total;
}



sub average_ani_old{
	my @list=<$_[0]/*.fna>;
	my $out_tmp="tmp_ani.coords";
	my $n=0;
	my $total=0;
	for (my $i=0;$i<=$#list;$i++){
		my $g1=$list[$i];
		for (my $j=$i+1;$j<=$#list;$j++){
			my $g2=$list[$j];
			my $t="NA";
			my $out=`ani.rb -1 $g1 -2 $g2 -n 0 --threads 16`;
			foreach my $line (split("\n",$out)){
				if ($line=~/Two-way ANI  : ([\d\.]+)%/){$t=$1;}
			}
			if ($t eq "NA"){
				print "No ANI between $g1 and $g2 ?!?!?!?! \n";
				die("\n");
			}
			$total+=$t;
			$n++;
		}
	}
	$total/=$n;
	return $total;
}


sub get_power_distrib(){
	my $n_seq=$_[0];
	my $n_power=$_[1];
	my $stop=0.99;
	my $step=($stop-0.01)/$n_seq;
	my @tab;
	for (my $i=0.01;$i<$stop;$i+=$step){
		push(@tab,$i**$n_power);
	}
	@tab=reverse(@tab);
	my $n=scalar(@tab);
	print "we asked for $n_seq values, we got $n\n";
	if ($n>$n_seq){@tab=@tab[0..$n_seq-1];}
	@tab=@{&normalize_distrib(\@tab)};
	return(\@tab);
}



sub normalize_distrib(){ # to get all distributions on the same number of observations
	my @tab=@{$_[0]};
	my $total=0;
	foreach my $value(@tab){$total+=$value;}
	for(my $i=0;$i<=$#tab;$i++){$tab[$i]/=$total;}
	return \@tab;
}


sub test_unpaired{
	my $test=`gunzip -c $_[0] | wc -l`;
	chop($test);
	my $test_2=`gunzip -c $_[1] | wc -l`;
	chop($test_2);
	$test+=$test_2;
	if ($test==0){return 1}
	else{return $test}
}
