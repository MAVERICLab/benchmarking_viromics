#!/usr/bin/perl
use strict;
use autodie;
use Getopt::Long;
my $h='';
my $out_dir="Simulated_files_AssemblyBenchmark/";
GetOptions ('help' => \$h, 'h' => \$h);
if ($h==1 || $ARGV[0] eq ""){ # If asked for help or did not set up any argument
	print "# Script to generate the composition files for the 15 simulate viromes
# Arguments toto \n";
	die "\n";
}
my $index="index";
my $read_len=100; 
my $min_powerlaw=1;
my $max_powerlaw=50;

# First get the list of viruses 
my %check;
my %store_len;
my %id_to_name;
open my $tsv,"<",$index;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	if ($tab[3]=~/phage/){
		$check{$tab[0]}=1;
		$store_len{$tab[0]}=$tab[1];
		$id_to_name{$tab[0]}=$tab[3];
	}
}
close $tsv;
my @tab_viruses=sort keys %check;
my $n=$#tab_viruses+1;
print "$n phage genomes\n";

my @tab_samples;
# For each sample
my %check_abundant;

my $sample_counter=1;
for (my $i=0;$i<=4;$i++){
	my $sample_id="Sample_".$sample_counter;
	push(@tab_samples,$sample_id);
	$sample_counter++;
	my $out_file=$sample_id."_comp.tsv";
	# First get the number of sequences(random, between 500 & 1000)
	my $nb_seq=500+int(rand()*500);
	&fisher_yates_shuffle(\@tab_viruses);
	my @tab_v_s;
	my %cover;
	# take the index of the power law curve randomly between 1 and 30
	my $power_law=$min_powerlaw+int(rand()*$max_powerlaw);
	print "## $sample_id -> $out_file - $nb_seq - power law index : $power_law\n";
	# Then get the power law distribution
	# and the list of sequences
	my @tab_cover=@{&get_power_distrib($nb_seq,$power_law)};
	for (my $j=0;$j<=$#tab_cover;$j++){
		push(@tab_v_s,$tab_viruses[$j]);
		$cover{$tab_viruses[$j]}=$tab_cover[$j];
	}
	@tab_v_s=sort {$cover{$b} <=> $cover{$a} } @tab_v_s;
	if ($i==0){
		# This is the first of the group of four, we just list our 50 abundant
		for (my $j=0;$j<50;$j++){
			$check_abundant{$tab_v_s[$j]}=1;
		}
	}
	else{
		# this is not the first sample, we reshuffle
		my @tab_to_replace;
		my @potential_replacement;
		my %check_seen;
		my $nb_dom_shared=40+int(rand()*10);
		my $shared=0;
		for (my $j=0;$j<50;$j++){
			if($check_abundant{$tab_v_s[$j]}==1){
				$shared++;
				$check_seen{$tab_v_s[$j]}=1;
			}
			else{push(@tab_to_replace,$j);}
		}
		# Then replace to create the fake beta diversity (if needed)
		print "We want $nb_dom_shared shared in the dominant, we currently have $shared\n";
		for (my $j=50;$j<=$#tab_v_s;$j++){
			if($check_abundant{$tab_v_s[$j]}==1){
				push(@potential_replacement,$j);
				$check_seen{$tab_v_s[$j]}=1;
			}
		}
		my $n=$#potential_replacement+1;
		print "We have $n potential replacements\n";
		my $todo=$nb_dom_shared-$shared;
		if ($todo>$n){
			&fisher_yates_shuffle(\@potential_replacement);
			&fisher_yates_shuffle(\@tab_to_replace);
			for (my $j=0;$j<$n;$j++){
# 				print "$tab_v_s[$tab_to_replace[$j]] will be replaced by $tab_v_s[$potential_replacement[$j]]\n";
				my $t=$cover{$tab_v_s[$tab_to_replace[$j]]};
				$cover{$tab_v_s[$tab_to_replace[$j]]}=$cover{$tab_v_s[$potential_replacement[$j]]};
				$cover{$tab_v_s[$potential_replacement[$j]]}=$t;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
			}
			my @tab_others;
			foreach my $seq (keys %check_abundant){
				if ($check_seen{$seq}==1){}
				else{
					push(@tab_others,$seq);
				}
			}
			&fisher_yates_shuffle(\@tab_others);
			for (my $j=$n;$j<$todo;$j++){
				my $index=$j-$n;
# 				print "$tab_v_s[$tab_to_replace[$j]] will be removed, and replaced by $tab_others[$index]\n";
				$tab_v_s[$tab_to_replace[$j]]=$tab_others[$index];
				$cover{$tab_others[$index]}=$cover{$tab_v_s[$tab_to_replace[$j]]};
			}
		}
		else{
			&fisher_yates_shuffle(\@potential_replacement);
			&fisher_yates_shuffle(\@tab_to_replace);
			for (my $j=0;$j<$todo;$j++){
# 				print "$tab_v_s[$tab_to_replace[$j]] will be replaced by $tab_v_s[$potential_replacement[$j]]\n";
				my $t=$cover{$tab_v_s[$tab_to_replace[$j]]};
				$cover{$tab_v_s[$tab_to_replace[$j]]}=$cover{$tab_v_s[$potential_replacement[$j]]};
				$cover{$tab_v_s[$potential_replacement[$j]]}=$t;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
			}
		}
	}
	@tab_v_s=sort {$cover{$b} <=> $cover{$a} } @tab_v_s;
	# Now we check all the abundant
	for (my $j=0;$j<50;$j++){
		$check_abundant{$tab_v_s[$j]}=1;
	}
	# We generate the output
	open my $s1,">",$out_file;
	print $s1 "## Virus\tName\tCoverage\n";
	my %seen;
	foreach my $seq (@tab_v_s){
		print $s1 "$seq\t$id_to_name{$seq}\t$cover{$seq}\n";
		if ($seen{$seq}==1){
			print "Already seen $seq ???\n";
# 			<STDIN>;
		}
		$seen{$seq}=1;
	}
	close $s1;
}


# Second, the three trios -> never any overlap on the 50 dominant
for (my $pi=0;$pi<=2;$pi++){
	print "## Group $pi\n";
	my %check_abundant_pair;
	my @tab_generated;
	for (my $i=0;$i<=2;$i++){
		my $sample_id="Sample_".$sample_counter;
		push(@tab_samples,$sample_id);
		$sample_counter++;
		my $out_file=$sample_id."_comp.tsv";
		# First get the number of sequences(random, between 500 & 1000)
		my $nb_seq=500+int(rand()*500);
		&fisher_yates_shuffle(\@tab_viruses);
		my @tab_v_s;
		my %cover;
		# take the index of the power law curve randomly between 1 and $max_powerlaw
		my $power_law=$min_powerlaw+int(rand()*$max_powerlaw);
		print "## $sample_id -> $out_file - $nb_seq - power law index : $power_law\n";
		# Then get the power law distribution
		# and the list of sequences
		my @tab_cover=@{&get_power_distrib($nb_seq,$power_law)};
		for (my $j=0;$j<=$#tab_cover;$j++){
			push(@tab_v_s,$tab_viruses[$j]);
			$cover{$tab_viruses[$j]}=$tab_cover[$j];
		}
		@tab_v_s=sort {$cover{$b} <=> $cover{$a} } @tab_v_s;
		# Get some potential replacement to the dominant that would need to be replaced because already dominant
		my @tab_to_replace;
		my $shared=0;
		for (my $j=0;$j<50;$j++){
			if ($check_abundant{$tab_v_s[$j]}==1){
				push(@tab_to_replace,$j);
			}
		}
		my @potential_replacement;
		for (my $j=50;$j<=$#tab_v_s;$j++){
			if($check_abundant{$tab_v_s[$j]}==1){}
			else{
				push(@potential_replacement,$j);
			}
		}
		&fisher_yates_shuffle(\@potential_replacement);
		&fisher_yates_shuffle(\@tab_to_replace);
		my $todo=scalar(@tab_to_replace);
		for (my $j=0;$j<$todo;$j++){
			print "$tab_v_s[$tab_to_replace[$j]] will be replaced by $tab_v_s[$potential_replacement[$j]] because the former is already abundant in other datasets\n";
			my $t=$cover{$tab_v_s[$tab_to_replace[$j]]};
			$cover{$tab_v_s[$tab_to_replace[$j]]}=$cover{$tab_v_s[$potential_replacement[$j]]};
			$cover{$tab_v_s[$potential_replacement[$j]]}=$t;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
		}
		if ($i==0){
			for (my $j=0;$j<50;$j++){
				$check_abundant_pair{$tab_v_s[$j]}=1;
			}
		}
		else{
			# now making sure that both pairs shared
			my $nb_dom_shared=30+int(rand()*20);
			my %check_seen;
			my $shared=0;
			my @tab_to_replace;
			for (my $j=0;$j<50;$j++){
				if($check_abundant_pair{$tab_v_s[$j]}==1){
					$shared++;
					$check_seen{$tab_v_s[$j]}=1;
				}
				else{
					push(@tab_to_replace,$j);
				}
			}
			# Then replace to create the fake beta diversity (if needed)
			print "We want $nb_dom_shared shared in the dominant, we currently have $shared\n";
			my @potential_replacement;
			for (my $j=50;$j<=$#tab_v_s;$j++){
				if($check_abundant_pair{$tab_v_s[$j]}==1){
					push(@potential_replacement,$j);
					$check_seen{$tab_v_s[$j]}=1;
				}
			}
			my $n=$#potential_replacement+1;
			print "We have $n potential replacements\n";
			my $todo=$nb_dom_shared-$shared;
			if ($todo>$n){
				&fisher_yates_shuffle(\@potential_replacement);
				&fisher_yates_shuffle(\@tab_to_replace);
				for (my $j=0;$j<$n;$j++){
# 					print "$tab_v_s[$tab_to_replace[$j]] will be replaced by $tab_v_s[$potential_replacement[$j]]\n";
					my $t=$cover{$tab_v_s[$tab_to_replace[$j]]};
					$cover{$tab_v_s[$tab_to_replace[$j]]}=$cover{$tab_v_s[$potential_replacement[$j]]};
					$cover{$tab_v_s[$potential_replacement[$j]]}=$t;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
				}
				my @tab_others;
				foreach my $seq (keys %check_abundant_pair){
					if ($check_seen{$seq}==1){}
					else{
						push(@tab_others,$seq);
					}
				}
				&fisher_yates_shuffle(\@tab_others);
				for (my $j=$n;$j<$todo;$j++){
					my $index=$j-$n;
# 					print "$tab_v_s[$tab_to_replace[$j]] will be removed, and replaced by $tab_others[$index]\n";
					$tab_v_s[$tab_to_replace[$j]]=$tab_others[$index];
					$cover{$tab_others[$index]}=$cover{$tab_v_s[$tab_to_replace[$j]]};
				}
			}
			else{
				&fisher_yates_shuffle(\@potential_replacement);
				&fisher_yates_shuffle(\@tab_to_replace);
				for (my $j=0;$j<$todo;$j++){
					print "$tab_v_s[$tab_to_replace[$j]] will be replaced by $tab_v_s[$potential_replacement[$j]]\n";
					my $t=$cover{$tab_v_s[$tab_to_replace[$j]]};
					$cover{$tab_v_s[$tab_to_replace[$j]]}=$cover{$tab_v_s[$potential_replacement[$j]]};
					$cover{$tab_v_s[$potential_replacement[$j]]}=$t;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
				}
			}
		}
		@tab_v_s=sort {$cover{$b} <=> $cover{$a} } @tab_v_s;
		# We generate the output
		open my $s1,">",$out_file;
		print $s1 "## Virus\tName\tCoverage\n";
		my %seen;
		foreach my $seq (@tab_v_s){
			print $s1 "$seq\t$id_to_name{$seq}\t$cover{$seq}\n";
			if ($seen{$seq}==1){
				print "Already seen $seq ???\n";
				<STDIN>;
			}
			$seen{$seq}=1;
		}
		close $s1;
		push(@tab_generated,\@tab_v_s);
	}
	foreach my $p_tab (@tab_generated){
		for (my $j=0;$j<50;$j++){
			$check_abundant{$$p_tab[$j]}=1;
		}
	}	
}
# Generate files for computing alpha and beta diversity
# Global Species counts matrix
my %store;
foreach my $sample (@tab_samples){
	my $cover_file=$sample."_comp.tsv";
	open my $tsv,"<",$cover_file;
	while(<$tsv>){
		chomp($_);
		if ($_=~/^##/){next;}
		my @tab=split("\t",$_);
		$store{$tab[0]}{$sample}=$tab[2];
	}
	close $tsv;
}

my $global_out_file="Genome_coverage_matrix.csv";
open my $s1,">",$global_out_file;
my $f_l=join(",",@tab_samples);
print $s1 "$f_l\n";
foreach my $genome (sort keys %store){
	my $l=$genome;
	foreach my $sample (@tab_samples){
		if (defined($store{$genome}{$sample})){$l.=",".$store{$genome}{$sample};}
		else{$l.=",0";}
	}
	print $s1 "$l\n";
}
close $s1;



sub fisher_yates_shuffle {
	my $deck = shift;  # $deck is a reference to an array
	return unless @$deck; # must not be empty!
	my $i = @$deck;
	while (--$i) {
		my $j = int rand ($i+1);
		@$deck[$i,$j] = @$deck[$j,$i];
	}
}


sub get_power_distrib(){
	my $n_seq=$_[0];
	my $n_power=$_[1];
	my $stop=0.99;
	my $step=($stop-0.01)/$n_seq;
	my @tab;
	for (my $i=0.01;$i<$stop;$i+=$step){
		push(@tab,$i**$n_power);
	}
	@tab=reverse(@tab);
	my $n=scalar(@tab);
	@tab=@{&normalize_distrib(\@tab)};
	print "we asked for $n_seq values, we got $n\n";
	return(\@tab);
}


sub normalize_distrib(){ # to get all distributions on the same number of observations
	my @tab=@{$_[0]};
	my $total=0;
	foreach my $value(@tab){$total+=$value;}
	for(my $i=0;$i<=$#tab;$i++){$tab[$i]/=$total;}
	return \@tab;
}
