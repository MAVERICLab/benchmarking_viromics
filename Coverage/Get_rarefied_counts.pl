#!/usr/bin/env perl
use strict;
use autodie;
use Getopt::Long;
use Custom::Utils;
use EnvironmentModules;
module('unload samtools');
module('load samtools/1.3');
module('load bbtools');
module('load python');
use Cwd;
my $h='';
my $out_dir="Abundance_matrices/";
my $in_dir="Coverage/";
my $bam_dir="Bam_files/";
my $th_id=90;
my $th_cov=75;
my $th_size=10000;
GetOptions ('help' => \$h, 'h' => \$h, 'c=s'=>\$in_dir, 'd=s'=>\$out_dir, 'b=s'=>\$bam_dir);
if ($h==1 || $ARGV[0] eq ''){ # If asked for help or did not set up any argument
	print "# Script to get rarefied read counts
# Arguments : toto
# -d : output directory (default Abundance_matrices/)
# -c : coverage directory (default Coverage/)
# -b : mapping directory (default Bam_files/)\n";
	die "\n";
}
chdir("Mapping/");


my $nb_reads_max="";
my $lib_size_file=$out_dir."Library_size.csv";
open my $csv,"<",$lib_size_file;
while(<$csv>){
	chomp($_);
	if ($_=~/^#/){next;}
	my @tab=split(",",$_);
	if ($nb_reads_max eq ""){$nb_reads_max=$tab[1];}
	elsif($tab[1]<$nb_reads_max){$nb_reads_max=$tab[1];}
}
close $csv;

print "### Number of reads we will rarefy to: $nb_reads_max\n";

print "Now calculating the ratio of reads which will be subsampled for each sample\n";
my %store_sub;
open my $csv,"<",$lib_size_file;
while(<$csv>){
	chomp($_);
	if ($_=~/^#/){next;}
	my @tab=split(",",$_);
	$store_sub{$tab[0]}=$nb_reads_max/$tab[1];
}
close $csv;


my @list_bam=<$bam_dir/*contigs.bam>;
foreach my $bam_file (@list_bam){
	$bam_file=~/.*\/(Sample_\d+)_vs.*/;
	my $sample=$1;
	my $out_bam=$bam_dir."/".$sample."_vs_nr_contigs-rarefied.bam";
	if (-e $out_bam){print "$out_bam already here\n";next;}
	if ($store_sub{$sample}==1){
		print "$sample is at max, we don't subsample and we take all the reads\n";
		&run_cmd("cp $bam_file $out_bam");
	}
	else{
		&run_cmd("reformat.sh in=$bam_file out=$out_bam samplerate=$store_sub{$sample}");
	}
}

&run_cmd("sort_and_index_bam_vr.pl -i $bam_dir/");
&run_cmd("parse_coverage_vr_with_th-nm.py -b $bam_dir -o $in_dir/$th_id/ --th $th_id");


my %store_cover;
my @tab_samples;
my @list_cover=<$in_dir/$th_id/*rarefied*.csv>;
print "Reading rarefied coverage ... \n";
foreach my $file (@list_cover){
	$file=~/Coverage.*\/+(\d+)\/(Sample_\d+)_.*/;
	my $th=$1;
	my $sample=$2;
	push(@tab_samples,$sample);
	print "\t reading file $file - $sample ($th)\n";
	my %store_temp;
	my $i=0;
	open my $csv,"<",$file;
	while(<$csv>){
		chomp($_);
		if ($_=~/^,/){next;}
		my @tab=split(",",$_);
		if ($tab[1]>=$th_size && $tab[2]>=$th_cov){
			$store_cover{$tab[0]}{$sample}{"nb_reads"}=$tab[3];
		}
	}
	close $csv;
}

print "Writing abundance matrices ...\n";
my $out_file_1=$out_dir."Read_counts-rarefied.csv";
@tab_samples=sort(@tab_samples);
my $f_l=join(",",@tab_samples);
open my $s1,">",$out_file_1;
print $s1 "$f_l\n";
foreach my $pop (sort keys %store_cover){
	my $line_1=$pop;
	my $line_2=$pop;
	foreach my $sample(@tab_samples){
		if (!defined($store_cover{$pop}{$sample}{"nb_reads"})){$store_cover{$pop}{$sample}{"nb_reads"}=0;}
		$line_1.=",".$store_cover{$pop}{$sample}{"nb_reads"};
	}
	print $s1 "$line_1\n";
}
close $s1;
