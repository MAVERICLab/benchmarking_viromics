#!/usr/bin/env perl
use strict;
use autodie;
use Getopt::Long;
use Custom::Utils;
use EnvironmentModules;
module('load bbtools');
module('load python');
use Cwd;
my $h='';
my $n_cpu=8;
GetOptions ('help' => \$h, 'h' => \$h);
if ($h==1 || $ARGV[0] eq ''){ # If asked for help or did not set up any argument
	print "# Script to generate the abundance matrices ## FOR FIGURE 3 ##
# Arguments : toto
";
	die "\n";
}

chdir("Mapping");
# First load the affiliation of each contig
my %info;
my $contig_pool="Spades_10M_contigs_500_95-80.fna";
print "Reading contig pool $contig_pool ... \n";
open my $fa,"<",$contig_pool;
while(<$fa>){
	chomp($_);
	if ($_=~/^>(.*)/){
		$info{$1}{"pool"}=1;
	}
}
close $fa;

my %affi_link;
print "Reading links contig to genome ... \n";
my @list_affi=<../Simulations_10M/Sample_*/QC/QC_Spades/final_contigs.fna.basics.csv>;
foreach my $file (@list_affi){
	$file=~/Simulations_10M\/(Sample_\d+)\/QC/;
	my $sample=$1;
	print "\t reading file $file - $sample\n";
	open my $csv,"<",$file;
	while(<$csv>){
		chomp($_);
		my @tab=split(",",$_);
		my $id=$sample."_".$tab[0];
		$affi_link{$id}=$tab[2];
	}
	close $csv;
}

my @list_input=<../Simulations_10M/Sample_*/Refs_coverage.csv>;
print "Reading  input coverage ... \n";
my %store_cover;
foreach my $file (@list_input){
	$file=~/Simulations_10M\/(Sample_\d+)\/Refs/;
	my $sample=$1;
	print "\t reading file $file - $sample\n";
	open my $csv,"<",$file;
	while(<$csv>){
		chomp($_);
		if ($_=~/^#/){next;}
		my @tab=split(",",$_);
		$store_cover{$sample}{$tab[0]}=$tab[4]*200/$tab[2];
	}
	close $csv;
}

my %cluster;
my @list_cluster=<../Simulations_10M/Sample_*/Refs_clean_80-80.clstr>;
print "Reading links genome to genome ... \n";
foreach my $file (@list_cluster){
	$file=~/Simulations_10M\/(Sample_\d+)\/Refs/;
	my $sample=$1;
	print "\t reading file $file - $sample\n";
	open my $fts,"<",$file;
	my $c_seed="";
	while(<$fts>){
		chomp($_);
		if ($_=~/^>Cluster_\d+\t(\S+)/){$c_seed=$1;}
		else{
			my @tab=split("\t",$_);
			if ($tab[1]>99){
				my @t=($c_seed,keys %{$cluster{$c_seed}});
				foreach my $g (@t){
					$cluster{$tab[0]}{$g}=1;
					$cluster{$g}{$tab[0]}=1;
				}
			}
		}
	}
	close $fts;
}

print "Reading links contigs to contigs ... \n";
my $contig_cluster_file="Spades_10M_contigs_500_95-80.clstr";
my $c_seed="";
my %seed_to_clustered;
print "\t reading file $contig_cluster_file\n";
open my $fts,"<",$contig_cluster_file;
my $c_seed="";
while(<$fts>){
	chomp($_);
	if ($_=~/^>Cluster_\d+\t(\S+)/){$c_seed=$1;}
	else{
		my @tab=split("\t",$_);
		$seed_to_clustered{$c_seed}{$tab[0]}=1;
	}
}
close $fts;


# 1 file for each minimum size: 500 amnd 10000 (bbmap is for the type of mapping software, 1 is for the minimum coverage of a genome to be "expected")
# filename of the form: Sensitivity_Specificity_bbmap_1_",size,".csv"
my %store_recover;
my %store_len;
my @list_cover=<Coverage/*/*.csv>;
print "Reading coverage ... \n";
foreach my $file (@list_cover){
	$file=~/Coverage\/(\d+)\/(Sample_\d+)_.*/;
	my $th=$1;
	my $sample=$2;
	print "\t reading file $file - $sample ($th)\n";
	open my $csv,"<",$file;
	while(<$csv>){
		chomp($_);
		if ($_=~/^,/){next;}
		my @tab=split(",",$_);
		$store_recover{$sample}{$tab[0]}{$th}=$tab[2];
		$store_len{$tab[0]}=$tab[1];
	}
	close $csv;
}


my $out_file="Sensitivity_Specificity_bbmap_1_500.csv";
my $out_file_2="Sensitivity_Specificity_bbmap_1_10000.csv";
open my $s1,">",$out_file;
print $s1 "Sample,Th_coverage,Th_identity,Sensitivity,Fdr,Contig_by_genome\n";
open my $s2,">",$out_file_2;
print $s2 "Sample,Th_coverage,Th_identity,Sensitivity,Fdr,Contig_by_genome\n";

my %count;
my %count_by_genome;
my %seen;
my %tag;
my %genome_list;
my @tab_id=(0,80,90,95,98);
my @tab_cover_levels=(0,50,75,80,90,95,98,99);
my %store_nb_contigs;
print "Calculating sensitivity and specificity ...\n";
foreach my $sample (sort keys %store_cover){
	%count=();
	%seen=();
	%count_by_genome=();
	foreach my $contig (keys %{$store_recover{$sample}}){
		%tag=();
		%genome_list=();
		print "$contig is linked to $affi_link{$contig}\n";
		if (defined($seed_to_clustered{$contig})){
			foreach my $contig_clust (keys %{$seed_to_clustered{$contig}}){
				if ($affi_link{$contig_clust} ne $affi_link{$contig}){
					if ($store_cover{$sample}{$affi_link{$contig_clust}}>0){
						$genome_list{$affi_link{$contig_clust}}=1;
					}
				}
			}
		}
		my @t_genome=($affi_link{$contig},sort keys %genome_list);
		foreach my $genome (@t_genome){
			if (defined($cluster{$genome})){
				foreach my $additional (keys %{$cluster{$genome}}){
					if ($additional ne $affi_link{$contig}){
						if ($store_cover{$sample}{$additional}>0){
							$genome_list{$additional}=1;
						}
					}
				}
			}
		}
		@t_genome=($affi_link{$contig},keys %genome_list); # Just to make sure that the first genome is always the one that was associated with the seed contig
		foreach my $genome (@t_genome){
			print "\tcontig $contig -> $genome -> $store_cover{$sample}{$genome}\n";
			foreach my $th (@tab_id){
				foreach my $th_cover (@tab_cover_levels){
					if (defined($store_recover{$sample}{$contig}{$th}) && $store_recover{$sample}{$contig}{$th}>=$th_cover){
						if ($store_len{$contig}>=500){
							if (!defined($seen{$genome}{"500"}{$th}{$th_cover})){
								if ($store_cover{$sample}{$genome}>0){
									print "We expected $genome - $store_cover{$sample}{$genome}\n";
									$count{"500"}{$th}{$th_cover}{"cov_0"}++;
									$tag{"500"}{$th}{$th_cover}=1;
									$seen{$genome}{"500"}{$th}{$th_cover}=1;
									$count_by_genome{"500"}{$th}{$th_cover}{$genome}++;
									if ($th==90 && $th_cover==75){$store_nb_contigs{$sample}{$genome}{$store_len{$contig}}++}
								}
							}
							else{
								if ($store_cover{$sample}{$genome}>0){
									$tag{"500"}{$th}{$th_cover}=1;
									$count_by_genome{"500"}{$th}{$th_cover}{$genome}++;
									if ($th==90 && $th_cover==75){$store_nb_contigs{$sample}{$genome}{$store_len{$contig}}++}
								}
								if ($store_cover{$sample}{$genome}>=1){
								}
								print "We already saw genome $genome for 500 - $th - $th_cover\n";
							}
						}
						if ($store_len{$contig}>=10000){
							if (!defined($seen{$genome}{"10000"}{$th}{$th_cover})){
								if ($store_cover{$sample}{$genome}>0){
									$count{"10000"}{$th}{$th_cover}{"cov_0"}++;
									$tag{"10000"}{$th}{$th_cover}=1;
									$seen{$genome}{"10000"}{$th}{$th_cover}=1;
									$count_by_genome{"10000"}{$th}{$th_cover}{$genome}++;
								}
								if ($store_cover{$sample}{$genome}>=1){
									$count{"10000"}{$th}{$th_cover}{"cov_1"}++;
								}
							}
							else{
								if ($store_cover{$sample}{$genome}>0){
									$tag{"10000"}{$th}{$th_cover}=1;
									$count_by_genome{"10000"}{$th}{$th_cover}{$genome}++;
								}
								print "We already saw genome $genome for 10000 - $th - $th_cover\n";
							}
						}
					}
				}
			}
		}
		foreach my $th (@tab_id){
			foreach my $th_cover (@tab_cover_levels){
				if (defined($store_recover{$sample}{$contig}{$th}) && $store_recover{$sample}{$contig}{$th}>=$th_cover){
					if ($store_len{$contig}>=500){
						if (!defined($tag{"500"}{$th}{$th_cover})){
							# We could not find any genome responsible for this contig -> it's a False Positive
							print "#### We could not find any genome responsible for this contig at $th - $th_cover - 500-> it's a False Positive ($store_recover{$sample}{$contig}{$th})\n";
							$count{"500"}{$th}{$th_cover}{"fp"}++;
						}
					}
					if ($store_len{$contig}>=10000){
						if (!defined($tag{"10000"}{$th}{$th_cover})){
							print "#### We could not find any genome responsible for this contig of 10kb or more at $th - $th_cover - 500--> it's a False Positive\n";
							$count{"10000"}{$th}{$th_cover}{"fp"}++;
						}
					}
				}
			}
		}
	}
	foreach my $genome (keys %{$store_cover{$sample}}){
		if ($store_cover{$sample}{$genome}>=1){
			$count{"total_1"}++;
		}
	}
	foreach my $th (@tab_id){
		foreach my $th_cover (@tab_cover_levels){
			print "\t$th - $th_cover\n";
			my $sens=$count{"500"}{$th}{$th_cover}{"cov_1"}/$count{"total_1"};
			print "\t".$count{"500"}{$th}{$th_cover}{"cov_1"}." / ".$count{"total_1"}." ".$sens."\n";
			my $fpr=$count{"500"}{$th}{$th_cover}{"fp"}/($count{"500"}{$th}{$th_cover}{"fp"}+$count{"500"}{$th}{$th_cover}{"cov_0"});
			print "\t".$count{"500"}{$th}{$th_cover}{"fp"}." / ".$count{"500"}{$th}{$th_cover}{"fp"}." + ".$count{"500"}{$th}{$th_cover}{"cov_0"}." ".$fpr."\n";
			my $avg=0;
			my $n=0;
			foreach my $genome (keys %{$count_by_genome{"500"}{$th}{$th_cover}}){$avg+=$count_by_genome{"500"}{$th}{$th_cover}{$genome};$n++;}
			print "$avg $n ";
			$avg/=$n;
			print "\t"."$avg\n";
			# Columns: 
			# Sample     Th_coverage    Th_identity  Sensitivity      Fpr     Contig_by_genome
			print $s1 "$sample,$th_cover,$th,$sens,$fpr,$avg\n";
			###
			my $sens=$count{"10000"}{$th}{$th_cover}{"cov_1"}/$count{"total_1"};
			my $fpr=$count{"10000"}{$th}{$th_cover}{"fp"}/($count{"10000"}{$th}{$th_cover}{"fp"}+$count{"10000"}{$th}{$th_cover}{"cov_0"});
			my $avg=0;
			my $n=0;
			foreach my $genome (keys %{$count_by_genome{"10000"}{$th}{$th_cover}}){$avg+=$count_by_genome{"10000"}{$th}{$th_cover}{$genome};$n++;}
			$avg/=$n;
			print $s2 "$sample,$th_cover,$th,$sens,$fpr,$avg\n";
# 			<STDIN>;
		}
	}
	print "## sample $sample done\n";
}
close $s1;
close $s2;

my $out_file_3="Nb_contig_by_genome_bbmap_75-90_spectrum.csv";
open my $s1,">",$out_file_3;
print $s1 "Sample,Th_size,Contig_by_genome\n";
foreach my $sample (keys %store_nb_contigs){
	for (my $i=500;$i<=20000;$i+=500){
		my $avg=0;
		my $n=0;
		foreach my $genome (keys %{$store_nb_contigs{$sample}}){
			my $tag=0;
			foreach my $size (keys %{$store_nb_contigs{$sample}{$genome}}){
				if ($size>=$i){$avg+=$store_nb_contigs{$sample}{$genome}{$size}; $tag=1;}
			}
			if ($tag==1){$n++;}
		}
		$avg/=$n;
		print $s1 $sample.",".$i.",".$avg."\n";
	}
}
close $s1;



