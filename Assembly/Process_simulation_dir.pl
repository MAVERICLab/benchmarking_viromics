#!/usr/bin/env perl
use strict;
use autodie;
use Getopt::Long;
use Cwd;
use EnvironmentModules;
module("load megahit/1.0.6");
module("load velvet/1.2.07_1");
module("load idba/1.1.1");
module("unload spades");
module("load spades/3.10.0");
module("unload gcc");
module("load gcc/6.3.0");
module("load python");

my $h='';
my $cmd='';
my $out='';
my $code='';
my $wdir='';
my $ncpu=16;
GetOptions ('help' => \$h, 'h' => \$h, 'i=s'=>\$wdir);
if ($h==1 || $wdir eq ''){ # If asked for help or did not set up any argument
	print "# Script to process a simulation directory
# Arguments :
# -i : simulation dir
";
	die "\n";
}

chdir($wdir);

my $current=`pwd`;
chop($current);
$wdir=$current."/";

print "Working dir - $wdir \n";
my $raw_r1=$wdir."Reads_raw_R1.fastq.gz";
my $raw_r2=$wdir."Reads_raw_R2.fastq.gz";

# Do the QC with trimmomatic
my $qc_r1=$wdir."Reads_QC_R1.fastq.gz";
my $qc_r2=$wdir."Reads_QC_R2.fastq.gz";
my $qc_u1=$wdir."Reads_QC_R1_unpaired.fastq.gz";
my $qc_u2=$wdir."Reads_QC_R2_unpaired.fastq.gz";
if (-e $qc_r1 && -e $qc_r2){print "QC already done\n";}
else{&run_cmd("java -Xms64m -jar ~/Utils/Trimmomatic-0.36/trimmomatic-0.36.jar PE -phred33 $raw_r1 $raw_r2 $qc_r1 $qc_u1 $qc_r2 $qc_u2 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:30 MINLEN:50");}

#### Make the series of QC assemblies
print "####### 1. QC assemblies ########\n";
my $merged_fq=$wdir."Reads_QC_merged.fastq.gz";
if (-e $merged_fq){print "QC reads already merged\n";}
else{&merge_fastq($qc_r1,$qc_r2,$merged_fq);}
my $merged_fq_se=$wdir."Reads_QC_merged_unpaired.fastq.gz";
if (-e $merged_fq_se){print "unpaired reads after QC already merged\n";}
else{
	&run_cmd("cat $qc_u1 > $merged_fq_se");
	&run_cmd("cat $qc_u2 >> $merged_fq_se");
}
if (!(-e $merged_fq_se)){`touch $merged_fq_se`;}


if (!(-d $wdir."QC")){&run_cmd("mkdir $wdir/QC");}
# Assemble QC with Metavelvet
my $dir_qc_metav=$wdir."QC/QC_Metavelvet";
if (-d ($dir_qc_metav)){print "QC MetaVelvet already done\n";}
# else{assembly_Metavelvet_split($qc_r1,$qc_r2,$dir_qc_metav);}
else{assembly_Metavelvet_interleaved($merged_fq,$merged_fq_se,$dir_qc_metav);}
# Assemble QC with Omega
my $dir_qc_omega=$wdir."QC/QC_Omega";
if (-d ($dir_qc_omega)){print "QC Omega already done\n";}
# else{assembly_Omega_split($qc_r1,$qc_r2,$dir_qc_omega);}
else{assembly_Omega_interleaved($merged_fq,$merged_fq_se,$dir_qc_omega);}
# Assemble QC with Idba_ud
my $dir_qc_idba=$wdir."QC/QC_Idba";
if (-d ($dir_qc_idba)){print "QC Idba already done\n";}
# else{assembly_Idba_split($qc_r1,$qc_r2,$dir_qc_idba);}
else{assembly_Idba_interleaved($merged_fq,$merged_fq_se,$dir_qc_idba);}
# Assemble QC with Megahit
my $dir_qc_megahit=$wdir."QC/QC_Megahit";
if (-d ($dir_qc_megahit)){print "QC Megathi already done\n";}
# else{assembly_Megahit_split($qc_r1,$qc_r2,$dir_qc_megahit);}
else{assembly_Megahit_interleaved($merged_fq,$merged_fq_se,$dir_qc_megahit);}
# Assemble QC with Spades
my $dir_qc_spades=$wdir."QC/QC_Spades";
if (-d ($dir_qc_spades)){print "QC Spades already done\n";}
else{assembly_Spades_interleaved($merged_fq,$merged_fq_se,$dir_qc_spades);}

die("STOP HERE, THIS WAS JUST TO (TRY TO) REDO THE QC WITH PERFECT MAPPING\n");

# Digital normalization
# "normalize-by-median"
print "######## 2. Digital normalization ########\n";
my $dnorm_pe=$wdir."Reads_DG.fastq.gz";
my $dnorm_se=$wdir."Reads_DG_unp.fastq.gz";
if (-e $dnorm_pe){print "$dnorm_pe already here\n";}
else{
	#normalize coverage to 20x
	&run_cmd("~/Utils/khmer/scripts/normalize-by-median.py -k 20 -C 20 -N 4 -x 5e8 -p --savegraph $wdir/normC20k20.kh $merged_fq");
	&run_cmd("~/Utils/khmer/scripts/normalize-by-median.py -C 20 --savegraph $wdir/normC20k20.kh --loadgraph $wdir/normC20k20.kh $merged_fq_se");
	&run_cmd("~/Utils/khmer/scripts/filter-abund.py --force -V $wdir/normC20k20.kh $wdir/Reads_QC_merged.fastq.gz.keep $wdir/Reads_QC_merged_unpaired.fastq.gz.keep");
	&run_cmd("~/Utils/khmer/scripts/extract-paired-reads.py $wdir/Reads_QC_merged.fastq.gz.keep.abundfilt");
	# then normalize coverage to 5x
	&run_cmd("~/Utils/khmer/scripts/normalize-by-median.py -C 5 -k 20 -N 4 -x 5e8 -s $wdir/normC5k20.kh -p $wdir/Reads_QC_merged.fastq.gz.keep.abundfilt.pe");
	&run_cmd("~/Utils/khmer/scripts/normalize-by-median.py -C 5 --savegraph $wdir/normC5k20.kh --loadgraph $wdir/normC5k20.kh $wdir/Reads_QC_merged.fastq.gz.keep.abundfilt.se $wdir/Reads_QC_merged_unpaired.fastq.gz.keep.abundfilt");

	&run_cmd("gzip -c $wdir/Reads_QC_merged.fastq.gz.keep.abundfilt.pe.keep > $dnorm_pe");
	&run_cmd("gzip -c $wdir/Reads_QC_merged.fastq.gz.keep.abundfilt.se.keep > $dnorm_se");
	&run_cmd("gzip -c $wdir/Reads_QC_merged_unpaired.fastq.gz.keep.abundfilt.keep >> $dnorm_se");
	# Cleaning
	&run_cmd("rm $wdir/Reads_QC_merged.fastq.gz.keep.abundfilt.pe $wdir/Reads_QC_merged.fastq.gz.keep.abundfilt.se $wdir/Reads_QC_merged_unpaired.fastq.gz.keep.abundfilt");
}
# Make the series of diginorm assemblies
#### Make the series of QC assemblies
print "####### 3. Diginorm assemblies ########\n";
if (!(-d $wdir."DG")){&run_cmd("mkdir $wdir/DG");}
# Assemble Diginorm with Metavelvet
my $dir_dg_metav=$wdir."DG/DG_Metavelvet";
if (-d ($dir_dg_metav)){print "DG MetaVelvet already done\n";}
else{assembly_Metavelvet_interleaved($dnorm_pe,$dnorm_se,$dir_dg_metav);}
# Assemble Diginorm with Omega
my $dir_dg_omega=$wdir."DG/DG_Omega";
if (-d ($dir_dg_omega)){print "DG Omega already done\n";}
else{assembly_Omega_interleaved($dnorm_pe,$dnorm_se,$dir_dg_omega);}
# Assemble Diginorm with Idba_ud
my $dir_dg_idba=$wdir."DG/DG_Idba";
if (-d ($dir_dg_idba)){print "DG Idba already done\n";}
else{assembly_Idba_interleaved($dnorm_pe,$dnorm_se,$dir_dg_idba);}
# Assemble Diginorm with Megahit
my $dir_dg_megahit=$wdir."DG/DG_Megahit";
if (-d ($dir_dg_megahit)){print "DG Megahit already done\n";}
else{assembly_Megahit_interleaved($dnorm_pe,$dnorm_se,$dir_dg_megahit);}
# Assemble Diginorm with Spades
my $dir_dg_spades=$wdir."DG/DG_Spades";
if (-d ($dir_dg_spades)){print "DG Spades already done\n";}
else{assembly_Spades_interleaved($dnorm_pe,$dnorm_se,$dir_dg_spades);}


# Partitioning
# Partition
print "####### 4. Partitioning ########\n";
# "do-partition"
my $partition_dir=$wdir."partition";
if (-d $partition_dir){print "Partitions already here\n";}
else{
	&run_cmd("~/Utils/khmer/sandbox/filter-below-abund.py $wdir/normC5k20.kh $dnorm_pe $dnorm_se");
	&run_cmd("mv Reads_DG.fastq.gz.below Reads_DG.fastq.gz.below.fq");
	&run_cmd("mv Reads_DG_unp.fastq.gz.below Reads_DG_unp.fastq.gz.below.fq");
	&run_cmd("~/Utils/khmer/scripts/do-partition.py  -k 32 -x 1e9 --threads 4 kak *.fastq.gz.below.fq");
	&run_cmd("~/Utils/khmer/scripts/extract-partitions.py -X 100000 kak *.part");
	my @list_partition=<kak*.fq>;
	&run_cmd("mkdir $partition_dir");
	my $i=0;
	foreach my $group_file (@list_partition){
		# Create partition dir
		my $pid="Partition_".$i;
		my $pid_dir=$partition_dir."/".$pid;
		&run_cmd("mkdir $pid_dir");
		&run_cmd("~/Utils/khmer/scripts/extract-paired-reads.py $group_file -d $pid_dir -f --gzip");
		foreach my $file (<$pid_dir/*.fq*>){&run_cmd("mv $file $file.fastq.gz")}
		$i++;
		
	}
}

# Make the series of partition assemblies
print "####### 5. Assembling partitions ########\n";
my @partitions=<$partition_dir/Partition*>;
foreach my $pdir (@partitions){
	# Assemble Partition with Metavelvet
	my $pe_file=(<$pdir/kak.group*.fq.pe.fastq.gz>)[0];
	my $se_file=(<$pdir/kak.group*.fq.se.fastq.gz>)[0];
	my $dir_p_metav=$pdir."/P_Metavelvet";
	if (-d ($dir_p_metav)){print "$pdir MetaVelvet already done\n";}
	else{assembly_Metavelvet_interleaved($pe_file,$se_file,$dir_p_metav);}
	# Assemble Partition with Omega
	my $dir_p_omega=$pdir."/P_Omega";
	if (-d ($dir_p_omega)){print "$pdir Omega already done\n";}
	else{assembly_Omega_interleaved($pe_file,$se_file,$dir_p_omega);}
	# Assemble Partition with Idba_ud
	my $dir_p_idba=$pdir."/P_Idba";
	if (-d ($dir_p_idba)){print "$pdir Idba already done\n";}
	else{assembly_Idba_interleaved($pe_file,$se_file,$dir_p_idba);}
	# Assemble Partition with Megahit
	my $dir_p_megahit=$pdir."/P_Megahit";
	if (-d ($dir_p_megahit)){print "$pdir Megahit already done\n";}
	else{assembly_Megahit_interleaved($pe_file,$se_file,$dir_p_megahit);}
	# Assemble Partition with Spades
	my $dir_p_spades=$pdir."/P_Spades";
	if (-d ($dir_p_spades)){print "$pdir Spades already done\n";}
	else{assembly_Spades_interleaved($pe_file,$se_file,$dir_p_spades);}
}
# Now checking that all the assemblies worked as expected
&check_completion($partition_dir);
# Now merging all the partitions results
my $out_metav=$partition_dir."/P_Metavelvet_contigs.fna";
&get_final($partition_dir,"P_Metavelvet/final_contigs.fna",$out_metav);
my $out_omega=$partition_dir."/P_Omega_contigs.fna";
&get_final($partition_dir,"P_Omega/final_contigs.fna",$out_omega);
my $out_idba=$partition_dir."/P_Idba_contigs.fna";
&get_final($partition_dir,"P_Idba/final_contigs.fna",$out_idba);
my $out_megahit=$partition_dir."/P_Megahit_contigs.fna";
&get_final($partition_dir,"P_Megahit/final_contigs.fna",$out_megahit);
my $out_spades=$partition_dir."/P_Spades_contigs.fna";
&get_final($partition_dir,"P_Spades/final_contigs.fna",$out_spades);


# Inflating partitions
print "####### 6. Inflating partitions ########\n";
# Inflated partitions
my $partition_dir=$wdir."partition_inflated";
if (-d $partition_dir){print "Inflated partitions already here\n";}
else{
	&run_cmd("~/Utils/khmer/sandbox/sweep-reads3.py -x 3e8 kak.*.fq $merged_fq");
	my @list_partition=<kak*.fq.sweep3>;
	&run_cmd("mkdir $partition_dir");
	my $i=0;
	foreach my $group_file (@list_partition){
		# Create partition dir
		my $pid="Partition_".$i;
		my $pid_dir=$partition_dir."/".$pid;
		&run_cmd("mkdir $pid_dir");
		&run_cmd("~/Utils/khmer/scripts/extract-paired-reads.py $group_file -d $pid_dir --gzip");
		foreach my $file (<$pid_dir/*.fq*>){&run_cmd("mv $file $file.fastq.gz")}
		$i++;
	}
	&run_cmd("rm kak.* *.keep *.below*");
}
# Make the series of inflated partitions assemblies
print "####### 7. Assembling inflated partitions ########\n";
my @partitions=<$partition_dir/Partition*>;
foreach my $pdir (@partitions){
	my $pe_file=(<$pdir/kak.group*.fq.sweep3.pe.fastq.gz>)[0];
	my $se_file=(<$pdir/kak.group*.fq.sweep3.se.fastq.gz>)[0];
	# Assemble Partition with Metavelvet
	my $dir_p_metav=$pdir."/P_Metavelvet";
	if (-d ($dir_p_metav)){print "$pdir MetaVelvet already done\n";}
	else{assembly_Metavelvet_interleaved($pe_file,$se_file,$dir_p_metav);}
	# Assemble Partition with Omega
	my $dir_p_omega=$pdir."/P_Omega";
	if (-d ($dir_p_omega)){print "$pdir Omega already done\n";}
	else{assembly_Omega_interleaved($pe_file,$se_file,$dir_p_omega);}
	# Assemble Partition with Idba_ud
	my $dir_p_idba=$pdir."/P_Idba";
	if (-d ($dir_p_idba)){print "$pdir Idba already done\n";}
	else{assembly_Idba_interleaved($pe_file,$se_file,$dir_p_idba);}
	# Assemble Partition with Megahit
	my $dir_p_megahit=$pdir."/P_Megahit";
	if (-d ($dir_p_megahit)){print "$pdir Megahit already done\n";}
	else{assembly_Megahit_interleaved($pe_file,$se_file,$dir_p_megahit);}
	# Assemble Partition with Spades
	my $dir_p_spades=$pdir."/P_Spades";
	if (-d ($dir_p_spades)){print "$pdir Spades already done\n";}
	else{assembly_Spades_interleaved($pe_file,$se_file,$dir_p_spades);}
}
# Now merging all the partitions results
my $out_metav=$partition_dir."/P_Metavelvet_contigs.fna";
&get_final($partition_dir,"P_Metavelvet/final_contigs.fna",$out_metav);
my $out_omega=$partition_dir."/P_Omega_contigs.fna";
&get_final($partition_dir,"P_Omega/final_contigs.fna",$out_omega);
my $out_idba=$partition_dir."/P_Idba_contigs.fna";
&get_final($partition_dir,"P_Idba/final_contigs.fna",$out_idba);
my $out_megahit=$partition_dir."/P_Megahit_contigs.fna";
&get_final($partition_dir,"P_Megahit/final_contigs.fna",$out_megahit);
my $out_spades=$partition_dir."/P_Spades_contigs.fna";
&get_final($partition_dir,"P_Spades/final_contigs.fna",$out_spades);

die("Here we stop for now\n");

# Low abundance k-mer trimming
my @tab_th=(2,5,20);
my $n=8;
my $kh_file=$wdir."/k20.kh";
if (-e $kh_file){print "$kh_file already there\n";}
else{&run_cmd("~/Utils/khmer/scripts/load-into-counting.py -k 20 -x 5e7 $kh_file $merged_fq");}
foreach my $th_kmer_coverage (@tab_th){
# 	filter-abund
	print "####### $n. Filtering low k-mer coverage at $th_kmer_coverage #######\n";
	&run_cmd("~/Utils/khmer/scripts/filter-abund.py --force -C $th_kmer_coverage -V $kh_file $merged_fq $merged_fq_se");
	&run_cmd("~/Utils/khmer/scripts/extract-paired-reads.py Reads_QC_merged.fastq.gz.abundfilt --gzip");
	my $df_pe="Reads_QC_merged.fastq.gz.abundfilt.pe.fastq.gz";
	&run_cmd("mv Reads_QC_merged.fastq.gz.abundfilt.pe $df_pe");
	my $df_se="Reads_QC_merged.fastq.gz.abundfilt.se.fastq.gz";
	&run_cmd("mv Reads_QC_merged.fastq.gz.abundfilt.se $df_se");
	&run_cmd("gzip -c Reads_QC_merged_unpaired.fastq.gz.abundfilt >> $df_se");
	$n++;
	print "####### $n. Assembling dataset filtered at $th_kmer_coverage #######\n";
	my $Fdir=$wdir."/F".$th_kmer_coverage;
	if (!(-d $Fdir)){&run_cmd("mkdir $Fdir");}
	my $dir_f_metav=$Fdir."/F".$th_kmer_coverage."_Metavelvet";
	if (-d ($dir_f_metav)){print "F$th_kmer_coverage MetaVelvet already done\n";}
	else{assembly_Metavelvet_interleaved($df_pe,$df_se,$dir_f_metav);}
	# Assemble Diginorm with Omega
	my $dir_f_omega=$Fdir."/F".$th_kmer_coverage."_Omega";
	if (-d ($dir_f_omega)){print "F$th_kmer_coverage Omega already done\n";}
	else{assembly_Omega_interleaved($df_pe,$df_se,$dir_f_omega);}
	# Assemble Diginorm with Idba_ud
	my $dir_f_idba=$Fdir."/F".$th_kmer_coverage."_Idba";
	if (-d ($dir_f_idba)){print "F$th_kmer_coverage Idba already done\n";}
	else{assembly_Idba_interleaved($df_pe,$df_se,$dir_f_idba);}
	# Assemble Diginorm with Megahit
	my $dir_f_megahit=$Fdir."/F".$th_kmer_coverage."_Megahit";
	if (-d ($dir_f_megahit)){print "F$th_kmer_coverage Megahit already done\n";}
	else{assembly_Megahit_interleaved($df_pe,$df_se,$dir_f_megahit);}
	# Assemble Diginorm with Spades
	my $dir_f_spades=$Fdir."/F".$th_kmer_coverage."_Spades";
	if (-d ($dir_f_spades)){print "F$th_kmer_coverage Spades already done\n";}
	else{assembly_Spades_interleaved($df_pe,$df_se,$dir_f_spades);}
}

sub assembly_Metavelvet_split{
	my $r1=$_[0];
	my $r2=$_[1];
	my $out_dir=$_[2];
	&run_cmd("velveth $out_dir 51 -fmtAuto -separate $r1 $r2");
	&run_cmd("velvetg $out_dir -exp_cov auto -ins_length 400");
	&run_cmd("~/Utils/MetaVelvet/meta-velvetg $out_dir -discard_chimera yes");
	&run_cmd("rm $out_dir/Sequences $out_dir/Roadmaps $out_dir/PreGraph $out_dir/Graph2 $out_dir/*LastGraph*");
	&run_cmd("ln -s $out_dir/meta-velvetg.contigs.fa $out_dir/final_contigs.fna");
}

sub assembly_Metavelvet_interleaved{
	my $pe=$_[0];
	my $se=$_[1];
	my $out_dir=$_[2];
	my $test=`gunzip -c $se | wc -l`;
	chop($test);
	if ($test==0){&run_cmd("velveth $out_dir 51 -fmtAuto -shortPaired -interleaved $pe");} # Velveth is not capable of dealing with empty input files
	else{&run_cmd("velveth $out_dir 51 -fmtAuto -shortPaired -interleaved $pe -short $se");}
	&run_cmd("velvetg $out_dir -exp_cov auto -ins_length 400");
	&run_cmd("~/Utils/MetaVelvet/meta-velvetg $out_dir -discard_chimera yes");
	&run_cmd("rm $out_dir/Sequences $out_dir/Roadmaps $out_dir/PreGraph $out_dir/Graph2 $out_dir/*LastGraph*");
	&run_cmd("ln -s $out_dir/meta-velvetg.contigs.fa $out_dir/final_contigs.fna");
}

sub assembly_Omega_split{
	my $r1=$_[0];
	my $r2=$_[1];
	my $out_dir=$_[2];
	if (!($out_dir=~/\/$/)){$out_dir.="/";}
	`mkdir $out_dir`;
	my $out_root=$out_dir."Omega";
	my $tmp_r1="temp_read_r1.fastq";
	my $tmp_r2="temp_read_r2.fastq";
	&run_cmd("gunzip -c $r1 > $tmp_r1");
	&run_cmd("gunzip -c $r2 > $tmp_r2");
	&run_cmd("~/Utils/omega/omega -pe $tmp_r1,$tmp_r2 -l 60 -f $out_root");
	&run_cmd("rm $tmp_r1 $tmp_r2 $out_dir/*sortedReads.fasta");
	&run_cmd("ln -s $out_dir/Omega_contigs4.fasta $out_dir/final_contigs.fna");
}


sub assembly_Omega_interleaved{
	my $pe=$_[0];
	my $se=$_[1];
	my $out_dir=$_[2];
	if (!($out_dir=~/\/$/)){$out_dir.="/";}
	`mkdir $out_dir`;
	my $out_root=$out_dir."Omega";
	my $tmp_pe="temp_read_pe.fastq";
	my $tmp_se="temp_read_se.fastq";
	&run_cmd("gunzip -c $pe > $tmp_pe");
	&run_cmd("gunzip -c $se > $tmp_se");
	my $test=`gunzip -c $se | wc -l`;
	chop($test);
	if ($test==0){&run_cmd("~/Utils/omega/omega -pe $tmp_pe -l 60 -f $out_root");} # Omega is not capable of dealing with empty input files
	else{&run_cmd("~/Utils/omega/omega -pe $tmp_pe -se $tmp_se -l 60 -f $out_root");}
	&run_cmd("rm $tmp_pe $tmp_se $out_dir/*sortedReads.fasta");
	&run_cmd("ln -s $out_dir/Omega_contigs4.fasta $out_dir/final_contigs.fna");
}

sub assembly_Idba_split{
	my $r1=$_[0];
	my $r2=$_[1];
	my $out_dir=$_[2];
	my $tmp_r1="temp_read_r1.fastq";
	my $tmp_r2="temp_read_r2.fastq";
	&run_cmd("gunzip -c $r1 > $tmp_r1");
	&run_cmd("gunzip -c $r2 > $tmp_r2");
	my $merged_fasta="tmp.fasta";
	&run_cmd("fq2fa --merge $tmp_r1 $tmp_r2 $merged_fasta");
	&run_cmd("~/Utils/idba-1.1.1/bin/idba_ud -r $merged_fasta -o $out_dir --pre_correction --num_threads $ncpu");
	&run_cmd("rm $tmp_r1 $tmp_r2 $merged_fasta $out_dir/kmer $out_dir/graph-*.* $out_dir/align-* $out_dir/local-*.* $out_dir/contig-*.*");
	&run_cmd("ln -s $out_dir/contig.fa $out_dir/final_contigs.fna");
}

sub assembly_Idba_interleaved{
	my $pe=$_[0];
	my $se=$_[1];
	my $out_dir=$_[2];
	my $tmp_pe="temp_read_pe.fastq";
	my $tmp_se="temp_read_se.fastq";
	&run_cmd("gunzip -c $pe > $tmp_pe");
	&run_cmd("gunzip -c $se > $tmp_se");
	my $merged_fasta="tmp.fasta";
	&run_cmd("fq2fa --paired $tmp_pe $merged_fasta");
	&add_to_fasta($tmp_se,$merged_fasta);
	&run_cmd("~/Utils/idba-1.1.1/bin/idba_ud -r $merged_fasta -o $out_dir --pre_correction --num_threads $ncpu");
	&run_cmd("rm $tmp_pe $tmp_se $merged_fasta $out_dir/kmer $out_dir/graph-*.* $out_dir/align-* $out_dir/local-*.* $out_dir/contig-*.*");
	&run_cmd("ln -s $out_dir/contig.fa $out_dir/final_contigs.fna");
}

sub assembly_Megahit_split{
	my $r1=$_[0];
	my $r2=$_[1];
	my $out_dir=$_[2];
	&run_cmd("megahit -1 $r1 -2 $r2 -o $out_dir -t $ncpu --presets meta");
	&run_cmd("rm -rf $out_dir/intermediate_contigs");
	&run_cmd("ln -s $out_dir/final.contigs.fa $out_dir/final_contigs.fna");
}

sub assembly_Megahit_interleaved{
	my $pe=$_[0];
	my $se=$_[1];
	my $out_dir=$_[2];
	&run_cmd("megahit --12 $pe -r $se -o $out_dir -t $ncpu --presets meta");
	&run_cmd("rm -rf $out_dir/intermediate_contigs");
	&run_cmd("ln -s $out_dir/final.contigs.fa $out_dir/final_contigs.fna");
}

sub assembly_Spades_split{
	my $r1=$_[0];
	my $r2=$_[1];
	my $out_dir=$_[2];
	&run_cmd("spades.py --meta -1 $r1 -2 $r2 -o $out_dir -t $ncpu");
	&run_cmd("rm -rf $out_dir/K21 $out_dir/K33 $out_dir/K55 $out_dir/misc $out_dir/first_pe_contigs.fasta $out_dir/before_rr.fasta $out_dir/assembly_graph.* $out_dir/*.fastg $out_dir/corrected");
	&run_cmd("ln -s $out_dir/contigs.fasta $out_dir/final_contigs.fna");
}

sub assembly_Spades_interleaved{
	my $pe=$_[0];
	my $se=$_[1];
	my $out_dir=$_[2];
	&run_cmd("spades.py --meta --12 $pe -s $se -o $out_dir -t $ncpu");
	&run_cmd("rm -rf $out_dir/K21 $out_dir/K33 $out_dir/K55 $out_dir/misc $out_dir/first_pe_contigs.fasta $out_dir/before_rr.fasta $out_dir/assembly_graph.* $out_dir/*.fastg $out_dir/split_input $out_dir/corrected");
	&run_cmd("ln -s $out_dir/contigs.fasta $out_dir/final_contigs.fna");
}

sub merge_fastq{
	my $r1=$_[0];
	my $r2=$_[1];
	my $out_file=$_[2];
	print "Merging reads from $r1 and $r2 into $out_file\n";
	open my $fq1, "gunzip -c $r1 |";
	open my $fq2, "gunzip -c $r2 |";
	open my $s1 ,"| gzip -c > $out_file";
	my $i=1;
	while(<$fq1>){
		print $s1 "\@read_$i/1\n";
		$_ = <$fq1>;
		print $s1 $_;
		$_ = <$fq1>;
		print $s1 $_;
		$_ = <$fq1>;
		print $s1 $_;
		
		$_ = <$fq2>;
		print $s1 "\@read_$i/2\n";
		$_ = <$fq2>;
		print $s1 $_;
		$_ = <$fq2>;
		print $s1 $_;
		$_ = <$fq2>;
		print $s1 $_;
		$i++;
	}
	close $fq1;
	close $fq2;
	close $s1;
}

sub add_to_fasta{
	my $fastq=$_[0];
	my $fasta=$_[1];
	open my $fa,">>",$fasta;
	open my $fq,"<",$fastq;
	while(<$fq>){
		chomp($_);
		$_=~/^@(.*)/;
		print $fa ">$1\n";
		$_=<$fq>;
		print $fa $_;
		$_=<$fq>;
		$_=<$fq>;
	}
	close $fq;
	close $fa;
}

sub run_cmd{
	my $cmd=$_[0];
	print "$cmd\n";
	my $out=`$cmd`;
	print "$out\n";
	return($out);
}

sub test_unpaired{
	my $test=`gunzip -c $_[0] | wc -l`;
	chop($test);
	my $test_2=`gunzip -c $_[1] | wc -l`;
	chop($test_2);
	$test+=$test_2;
	if ($test==0){return 1}
	else{return $test}
}

sub check_completion{
	my $dir=$_[0];
	my @list=<$dir/Partition_*>;
	foreach my $dp (@list){
		print "checking $dp ($dir)\n";
		my $mv_file=$dp."/P_Metavelvet/final_contigs.fna";
		if (-e $mv_file){}
		else{print "!!!!!!!!!!!!!!!!!!!!!!!!! PBLM => No Metavelvet in $dp == $mv_file\n";}
		my $om_file=$dp."/P_Omega/final_contigs.fna";
		if (-e $om_file){}
		else{print "!!!!!!!!!!!!!!!!!!!!!!!!! PBLM => No Omega in $dp == $om_file\n";}
		my $id_file=$dp."/P_Idba/final_contigs.fna";
		if (-e $id_file){}
		else{print "!!!!!!!!!!!!!!!!!!!!!!!!! PBLM => No Idba in $dp == $id_file\n";}
		my $me_file=$dp."/P_Megahit/final_contigs.fna";
		if (-e $me_file){}
		else{print "!!!!!!!!!!!!!!!!!!!!!!!!! PBLM => No Megahit in $dp == $me_file\n";}
		my $sp_file=$dp."/P_Spades/final_contigs.fna";
		if (-e $sp_file){}
		else{print "!!!!!!!!!!!!!!!!!!!!!!!!! PBLM => No Spades in $dp == $sp_file\n";}
	}
}

sub get_final{
	my $dir=$_[0];
	my $dir_2=$_[1];
	my $final=$_[2];
	open my $s1,">",$final;
	my @tab=<$dir/*/$dir_2>;
	foreach my $file (@tab){
		print "reading $file\n";
		if ($file=~/Partition_(\d+)/){
			my $p=$1;
			open my $fa,"<",$file;
			while(<$fa>){
				chomp($_);
				if ($_=~/^>(.*)/){
					print $s1 ">".$p."_".$1."\n";
				}
				else{
					print $s1 "$_\n";
				}
			}
			close $fa;
		}
		else{
			print "####%%%%#### Pblm with $file\n";
		}
	}
	close $s1;
}
