#!/usr/bin/env perl
use strict;
use autodie;
use Getopt::Long;
use Cwd;
use EnvironmentModules;
module('load mummer/3.23-64bit');
my $h='';
my $cmd='';
my $out='';
my $code='';
my $wdir='';
my $redo=1;
GetOptions ('help' => \$h, 'h' => \$h, 'i=s'=>\$wdir, 'r=i'=>\$redo);
if ($h==1 || $wdir eq ''){ # If asked for help or did not set up any argument
	print "# Script to parse a simulation directory
# Arguments :
# -i : simulation dir
# -r : if you want to recompute the csv coverage and basics already there
";
	die "\n";
}

$wdir=~/(Sample_\d+)/;
my $sample=$1;

my $th_length=500; # We'll only look at contigs 500 bp and larger

## If not already done, generate the multiple db we'll need for nucmer
my $dir_db=$wdir."Nucmer_db/";
print "#### Creating the nr DBs for nucmer ###\n";
if (-d $dir_db){print "$dir_db already here\n";}
else{
	&run_cmd("mkdir $dir_db");
	my $ref_file=$wdir."/Refs_clean.fna";
	&run_cmd("~/Utils/Scripts/Cluster_genomes.pl -f $ref_file -c 80 -i 80");
	my $out_file=$wdir."/Refs_clean_80-80.clstr";
	my %store;
	open my $fts,"<",$out_file;
	my $i=0;
	while(<$fts>){
		chomp($_);
		if ($_=~/>Cluster_\d+\s+(\S+)/){
			$i=0;
			$store{$1}=$i;
		}
		else{
			$i++;
			my @tab=split("\t",$_);
			$store{$tab[0]}=$i;
		}
	}
	close $fts;
	open my $s1,">",$dir_db."Refs_0.fna";
	open my $fa,"<",$ref_file;
	while(<$fa>){
		chomp($_);
		if ($_=~/^>(\S+)/){
			close $s1;
			if (!defined($store{$1})){
				print "!!! Pblm with $1 -> no ref group ?\n";
				<STDIN>;
			}
			print "$1 goes to $store{$1}\n";
			open $s1,">>",$dir_db."Refs_".$store{$1}.".fna";
			print $s1 "$_\n";
		}
		else{
			print $s1 "$_\n";
		}
	}
	close $fa;
	close $s1;
}

## Load the input coverage
my $cover_file=$wdir."Refs_coverage.csv";
my %info;
open my $csv,"<",$cover_file;
while(<$csv>){
	chomp($_);
	if ($_=~/^#/){next;}
	my @tab=split(",",$_);
	$info{$tab[0]}{"length"}=$tab[2];
	$info{$tab[0]}{"nb_reads"}=$tab[4];
}
close $csv;

my @list_db=<$dir_db/*.fna>;

## Then list all the potential contig files
my %info_c;
my @list_tools=("Metavelvet","Omega","Idba","Megahit","Spades");
# my @list_tools=("Spades");
foreach my $tool (@list_tools){
	print "## Checking results from $tool\n";
 	my @tab_contig_files=($wdir."QC/QC_".$tool."/final_contigs.fna",$wdir."DG/DG_".$tool."/final_contigs.fna",$wdir."F2/F2_".$tool."/final_contigs.fna",$wdir."F5/F5_".$tool."/final_contigs.fna",$wdir."F20/F20_".$tool."/final_contigs.fna",$wdir."partition/P_".$tool."_contigs.fna",$wdir."partition_inflated/P_".$tool."_contigs.fna");
	foreach my $contig_file (@tab_contig_files){
		## Print a warning if the file doesn't exist
		if (!(-e $contig_file)){print "## Results of $tool for $contig_file are not there yet\n";next;}
		$contig_file=~/\/Sample_\d+\/([^\/]+)\//;
		my $type=$1;
		print "contig file $contig_file $wdir -> $type\n";
		# List circular contigs if not already done
		my $circ_file=$contig_file.".circular.txt";
		if (-e $circ_file){}
		else{&circular_file($contig_file,$circ_file);}
		## If the nucmer coords file exists, use it, otherwise generate it and read it
		my $coord_file=$contig_file.".coords";
		if (-e $coord_file){print "$coord_file already here\n";}
		else{
			my $tmp_file="out.coords";
			if (-e $tmp_file){`rm $tmp_file`;}
			foreach my $db (@list_db){
				# Run nucmer
				&run_cmd("nucmer --maxmatch --coords --nooptimize --nosimplify $db $contig_file");
				# Cat to the coords file
				&run_cmd("cat $tmp_file >> $coord_file");
			}
		}
		my $cover_file=$contig_file.".coverage.csv";
		if (-e $cover_file && !($redo==1)){
			print "$cover_file already there and we were not told to redo everything, so we skip\n";
			next;
		}
		
		# Read circularity file
		%info_c=();
		open my $tsv,"<",$circ_file;
		while(<$tsv>){
			chomp($_);
			my @tab=split("\t",$_);
			$info_c{$tab[0]}{"length"}=$tab[1];
			$info_c{$tab[0]}{"type"}=$tab[2];
		}
		close $tsv;
		# and Read the coord file
		my %store;
		print "Reading coords file\n";
		open my $txt,"<",$coord_file;
		my $t=0;
		while(<$txt>){
			chomp($_);
			if ($_=~/^\/global\//){$t=0;}
			if ($_=~/^\=\=\=\=\=\=/){$t=1}
			elsif($t==1){
				my @tab=split(" ",$_);
				if ($info_c{$tab[12]}{"length"}<$th_length){next;} # We don't consider contigs smaller than th_length
				if ($tab[11] eq $tab[12]){next;} # same genome
				if ($tab[1]>$tab[0]){}
				else{
					my $temp=$tab[1];
					$tab[1]=$tab[0];
					$tab[0]=$temp;
				}
				if ($tab[4]>$tab[3]){}
				else{
					my $temp=$tab[4];
					$tab[4]=$tab[3];
					$tab[3]=$temp;
				}
				my $pcent=$tab[9];
				my $seed=$tab[11];
				my $seed_c=$tab[6];
				my $query=$tab[12];
				my $query_c=$tab[7];
				print "match between $query to $seed => $pcent - $seed_c / $query_c\n";
				if ($pcent>=90){
					for (my $i=$tab[0];$i<=$tab[1];$i++){
						if (defined($store{"genome"}{$seed}{$query}{$i})){}
						else{
							$store{"genome"}{$seed}{$query}{$i}++;
							$store{"genome"}{$seed}{$query}{"total"}++;
							$store{"genome"}{$seed}{"total"}{$i}++;
							$store{"genome"}{$seed}{"total"}{"total"}++;
						}
					}
					for (my $i=$tab[3];$i<=$tab[4];$i++){
						if (defined($store{"contig"}{$query}{$seed}{$i})){}
						else{
							$store{"contig"}{$query}{$seed}{$i}++;
							$store{"contig"}{$query}{$seed}{"total"}++;
						}
					}
				}
			}
		}
		close $txt;
		
		
		my $basic_file=$contig_file.".basics.csv";
		my %store_best;
		print "Calculating contigs coverage and detecting chimeras\n";
		open my $s1,">",$basic_file;
		foreach my $contig (sort keys %{$store{"contig"}}){
			# Headers -> Contig / Size / Best Hit / Best hit coverage / True or Chimera / Linear or Circular
			print "\t$contig\n";
			my @tab_g=sort {$store{"contig"}{$contig}{$b}{"total"} <=> $store{"contig"}{$contig}{$a}{"total"}} keys %{$store{"contig"}{$contig}};
			my $bh=$tab_g[0];
			if ($info_c{$contig}{"length"}==0){
				die("pblm, contig $contig has 0 length\n");
			}
			my $cover=$store{"contig"}{$contig}{$bh}{"total"}/$info_c{$contig}{"length"};
			if ($cover >= 0.95 ){
				# Genuine assembly
				print $s1 $contig.",".$info_c{$contig}{"length"}.",".$bh.",".$cover.",true,".$info_c{$contig}{"type"}."\n";
				$store_best{$contig}=$bh;
			}
			else{
				if ($#tab_g>0){
					print "$contig is covered by multiple genomes, but none at 95% or more, we consider this a chimera\n";
					print $s1 $contig.",".$info_c{$contig}{"length"}.",".$bh.",".$cover.",chimera,".$info_c{$contig}{"type"}."\n";
				}
				else{
					print $s1 $contig.",".$info_c{$contig}{"length"}.",".$bh.",".$cover.",true,".$info_c{$contig}{"type"}."\n";
				}
			}
		}
		close $s1;
		
		
		## Print the result files 
		# First coverage file
		# Columns are
		# Sample Genome Length Asb Asber Reads Circular Input_Coverage Recovery Largest_contig_coverage Large_contig_coverage
		print "Calculating recovery coverage\n";
		open my $s1,">",$cover_file;
		foreach my $genome (sort keys %info){
			print "\t$genome\n";
			if ($info{$genome}{"nb_reads"}==0){next;} # We don't bother with the genomes at coverage 0
			my $circular=0;
			my $recovery=0;
			my %check_recovery;
			my $large_recovery=0;
			my $largest_recovery=0;
			foreach my $contig (sort {$store{"genome"}{$genome}{$b}{"total"} <=> $store{"genome"}{$genome}{$a}{"total"}} keys %{$store{"genome"}{$genome}}){
				if ($contig eq "total"){next;}
				else{
					if ($largest_recovery==0){$largest_recovery=$store{"genome"}{$genome}{$contig}{"total"};}
					if ($info_c{$contig}{"type"} eq "circular"){
						if ($store_best{$contig} eq $genome){
							print "$contig is best for $genome and circular -> $genome gets circular ++\n";
							$circular++;
						}
					}
					foreach my $pos (keys %{$store{"genome"}{$genome}{$contig}}){
						if ($pos eq "total"){next;}
						if ($check_recovery{$pos}==1){next;}
						$check_recovery{$pos}=1;
						$recovery++;
						if ($info_c{$contig}{"length"}>=10000){$large_recovery++;}
					}
				}
			}
			$recovery/=$info{$genome}{"length"}/100;
			$large_recovery/=$info{$genome}{"length"}/100;
			$largest_recovery/=$info{$genome}{"length"}/100;
			print $s1 $sample.",".$genome.",".$info{$genome}{"length"}.",".$tool."_".$type.",".$tool.",".$type.",".$circular.",".$info{$genome}{"nb_reads"}.",".$recovery.",".$largest_recovery.",".$large_recovery."\n";
			
		}
		close $s1;
	}
}

sub run_cmd{
	my $cmd=$_[0];
	print "$cmd\n";
	my $out=`$cmd`;
	print "$out\n";
	return($out);
}



sub circular_file{
	my $fasta_contigs=$_[0];
	my $out_file=$_[1];
	open my $s1, '>', $out_file;
	open my $fa, '<', $fasta_contigs;
	my %seq_base;
	my $id_seq="";
	my $c_seq="";
	while(<$fa>){
		$_=~s/\r\n/\n/g; #Cas d'un fichier windows ##AJOUT
		chomp($_);
		if ($_=~/^>(\S*)/){
			my $id_temp=$1;
			if (length($c_seq)>0){
				if (&test_circularity($c_seq)==1){print $s1 "$id_seq\t".length($c_seq)."\tcircular\n";}
				else{print $s1 "$id_seq\t".length($c_seq)."\tlinear\n";}
			}
			$id_seq=$id_temp;
			$c_seq="";
		}
		else{$c_seq.=$_;}
	}
	#### Don't forget the last one !!!
	if (&test_circularity($c_seq)==1){print $s1 "$id_seq\t".length($c_seq)."\tcircular\n";}
	else{print $s1 "$id_seq\t".length($c_seq)."\tlinear\n";}
	### 
	close $fa;
	close $s1;
}

sub test_circularity {
	my $seq=$_[0];
	my $circu=0;
	my $prefix=substr($seq,0,10);
	if ($seq=~/(.+)($prefix.*?)$/){
 		print "We found the prefix ($prefix) further in the sequence\n";
		my $sequence=$1;
		my $suffix=$2;
		my $test=substr($seq,0,length($suffix));
		if ($suffix eq $test){
 			print " and it's matching suffixe $suffix, so we have a circular contig\n";
			$circu=1;
		}
	}
	return $circu;
}
